package local.bb.studio_projektowe;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.scene.room.items.Bomb;
import local.bb.studio_projektowe.scene.room.items.RoomItemFactory;
import local.bb.studio_projektowe.tools.RobotPersisted;

public class SPMain extends ApplicationAdapter {

	public static final int TEXTURE_SIZE = 20;
	public static final int SCREEN_WIDTH = 800;
	public static final int SCREEN_HEIGHT = 600;
	public static final int TIMEOUT = 350;
	public static final int NROBOTS = 3;
	
	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;

	private Texture unknownTexture;
	private Texture floorTexture;
	private Texture wallTexture;
	private Texture bombTexture;
	private Texture startingPointTexture;
	private Texture robotTexture;
	private Texture otherRobotTexture;
	
	int offsetWidth;
	int offsetHeight;
	
	private final Robot thisRobot;
	private ArrayList<RobotPersisted> robots = new ArrayList<>();

	public SPMain(Robot thisRobot) {
		this.thisRobot = thisRobot;
		addUpdateRobot(thisRobot);
	}

	@Override
	public void create () {
		
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();

		unknownTexture = new Texture(Gdx.files.internal("tiles/unknown.png"));
		floorTexture = new Texture(Gdx.files.internal("tiles/floor.png"));
		wallTexture = new Texture(Gdx.files.internal("tiles/wall.png"));
		bombTexture = new Texture(Gdx.files.internal("tiles/bomb.png"));
		startingPointTexture = new Texture(Gdx.files.internal("tiles/starting_point.png"));
		robotTexture = new Texture(Gdx.files.internal("tiles/robot.png"));
		otherRobotTexture = new Texture(Gdx.files.internal("tiles/other_robot.png"));

		offsetWidth = SCREEN_WIDTH/2-(thisRobot.getRows()/2*TEXTURE_SIZE);
		offsetHeight = SCREEN_HEIGHT/2-(thisRobot.getCols()/2*TEXTURE_SIZE);
	}
	
	public void addUpdateRobot(RobotPersisted robot) {
		for(RobotPersisted r : robots) {
			if(r.equals(robot)) {
				r.setPosition(robot.getPosition());
				return;
			}
		}
		robots.add(robot);
	}
	
	public void addUpdateRobot(Robot robot) {
		addUpdateRobot(robot.getRobotPersisted());
	}
	
	public ArrayList<RobotPersisted> getRobots() {
		return robots;
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		if(!robots.isEmpty()) {
			for(int i=0 ; i<thisRobot.getRows() ; ++i) {
				for(int j=0 ; j<thisRobot.getCols() ; ++j) {
					Texture tex;
					char code = thisRobot.getItems()[i][j].getCode();
					int x = offsetWidth+(thisRobot.getRows()-i)*TEXTURE_SIZE;
					int y = offsetHeight+j*TEXTURE_SIZE;
					if(code == RoomItemFactory.getCodeOf("floor")) {
						tex = floorTexture;
					} else if(code == RoomItemFactory.getCodeOf("wall")) {
						tex = wallTexture;
					} else if(code == RoomItemFactory.getCodeOf("bomb")) {
						tex = bombTexture;
					} else if(code == RoomItemFactory.getCodeOf("starting point")) {
						tex = startingPointTexture;
					} else {
						tex = unknownTexture;
					}
					
						/*
							batch.draw(
									floorTexture, 
									offsetHeight+j*TEXTURE_SIZE, 
									offsetWidth+(thisRobot.getRows()-i)*TEXTURE_SIZE,
									TEXTURE_SIZE,
									TEXTURE_SIZE);
							int defusedHeight = TEXTURE_SIZE;
							batch.draw(
									bombTexture, 
									offsetHeight+j*TEXTURE_SIZE, 
									offsetWidth+(thisRobot.getRows()-i)*TEXTURE_SIZE,
									TEXTURE_SIZE,
									TEXTURE_SIZE,
									TEXTURE_SIZE,
									defusedHeight,
									1,1,0,0,0,
									TEXTURE_SIZE,
									defusedHeight,
									false,false);
						*/
					batch.draw(
							tex,
							y,
							x,
							TEXTURE_SIZE,
							TEXTURE_SIZE);
					if(tex == bombTexture) {
						try{
							batch.end();
							Bomb bomb = (Bomb)thisRobot.getItems()[i][j];
							shapeRenderer.begin(ShapeType.Filled);
							/*
							shapeRenderer.setColor(Color.RED);
							shapeRenderer.rect(y, x, TEXTURE_SIZE/4, 
									TEXTURE_SIZE*(bomb.getExplosionTime()/bomb.getMaxExplosionTime()));
							*/
							shapeRenderer.setColor(Color.BLUE);
							float defuseBarSize = TEXTURE_SIZE*(bomb.getDefuseTime()/(float)bomb.getMaxDefuseTime());
							shapeRenderer.rect(y+(TEXTURE_SIZE/4*3), x, TEXTURE_SIZE/4, defuseBarSize);
							shapeRenderer.end();
							batch.begin();
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		for(RobotPersisted robot : robots) {
			TextureRegion tr = (robot.getId() == thisRobot.getId()) 
					? new TextureRegion(robotTexture) 
							: new TextureRegion(otherRobotTexture);
			int rotation = 0;
			switch(robot.getPosition().getDirection()) {
			case UP:
				rotation = -90;
				break;
			case LEFT:
				rotation = 180;
				break;
			case BOTTOM:
				rotation = 90;
				break;
			}
			batch.draw(
					tr, 
					offsetHeight+TEXTURE_SIZE*robot.getPosition().getCol(), 
					offsetWidth+TEXTURE_SIZE*(thisRobot.getRows()-robot.getPosition().getRow()), 
					TEXTURE_SIZE/2,
					TEXTURE_SIZE/2,
					TEXTURE_SIZE,
					TEXTURE_SIZE,
					1, 
					1, 
					rotation);
		}
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}

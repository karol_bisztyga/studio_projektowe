package local.bb.studio_projektowe.scene;

import local.bb.studio_projektowe.exceptions.RoomCreationException;
import local.bb.studio_projektowe.scene.room.Position;
import local.bb.studio_projektowe.scene.room.Room;
import local.bb.studio_projektowe.scene.room.items.Bomb;
import local.bb.studio_projektowe.scene.room.items.RoomItem;
import local.bb.studio_projektowe.scene.room.items.RoomItemFactory;
import local.bb.studio_projektowe.tools.RobotPersisted;
import local.bb.studio_projektowe.tools.RoomExplorer;
import local.bb.studio_projektowe.tools.communication.CommunicationManager;

public class Robot {
	
	public enum STATES {
		EXPLORING,
		APPROACHING,
		DEFUSING
	}
	
	private final String id;
	
	private Position position;
	private final Room room;
	private final RoomExplorer roomExplorer;
	private Bomb bombToDefuse = null;
	
	private final RoomItem[][] items;
	
	public Robot(String id, Room room, CommunicationManager communicationManager) 
			throws RoomCreationException {
		this(id, room, false, 0, communicationManager);
	}

	public Robot(String id, Room room, int roomExplorerOffset, CommunicationManager communicationManager) 
			throws RoomCreationException {
		this(id, room, false, roomExplorerOffset, communicationManager);
	}
	
	public Robot(String id, Room room, boolean dummy, int roomExplorerOffset, CommunicationManager communicationManager) 
			throws RoomCreationException {
		this.id = id;
		this.room = room;
		this.items = new RoomItem[room.getRows()][room.getCols()];
		for(int i=0 ; i<room.getRows() ; ++i) {
			for(int j=0 ; j<room.getCols() ; ++j) {
				items[i][j] = RoomItemFactory.getItem(RoomItemFactory.getCodeOf("unknown"));
			}
		}
		this.position = room.getStartingPosition();
		this.position.setDirection(room.getStartingDirection());
		items[position.getRow()][position.getCol()] = 
				RoomItemFactory.getItem(RoomItemFactory.getCodeOf("starting point"));
		roomExplorer = new RoomExplorer(this, dummy, roomExplorerOffset, communicationManager);
	}
	
	public void markBombAsDefused(int row, int col) {
		this.room.markBombAsDefused(row, col);
		this.items[row][col] = this.room.itemAt(row, col);
	}
	
	public void setItem(int row, int col, RoomItem item) {
		this.items[row][col] = item;
	}
	
	public void setItemFromRoom(int row, int col) {
		this.items[row][col] = room.itemAt(row, col);
	}
	
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Room getRoom() {
		return room;
	}

	public String getId() {
		return id;
	}

	public RoomItem[][] getItems() {
		return items;
	}

	public RoomItem getCurrentItem() {
		return getItems()[getPosition().getRow()][getPosition().getCol()];
	}

	public RoomExplorer getRoomExplorer() {
		return roomExplorer;
	}
	
	public int getRows() {
		return this.items.length;
	}
	
	public int getCols() {
		if(this.getRows() == 0) {
			return 0;
		}
		return this.items[0].length;
	}
	
	public RobotPersisted getRobotPersisted() {
		return new RobotPersisted(this.id, this.position);
	}

	public synchronized Bomb getBombToDefuse() {
		return bombToDefuse;
	}

	public synchronized void setBombToDefuse(Bomb bombToDefuse) {
		this.bombToDefuse = bombToDefuse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Robot other = (Robot) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}

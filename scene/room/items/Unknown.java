package local.bb.studio_projektowe.scene.room.items;

public class Unknown implements RoomItem {
	private static final long serialVersionUID = 1L;
	
	Unknown() {}

	@Override
	public char getCode() {
		return '?';
	}

}

package local.bb.studio_projektowe.scene.room.items;

public class RoomItemFactory {
	
	public static RoomItem getItem(char code) {
		switch(code) {
			case 'b':
				return new Bomb();
			case '+':
				return new Wall();
			case '-':
				return new Floor();
			case 's':
				return new StartingPoint();
			case '?':
				return new Unknown();
		}
		return null;
	}
	
	public static char getCodeOf(String itemName) {
		if(itemName.toLowerCase().equals("bomb")) {
			return 'b';
		}
		if(itemName.toLowerCase().equals("wall")) {
			return '+';
		}
		if(itemName.toLowerCase().equals("floor")) {
			return '-';
		}
		if(itemName.toLowerCase().equals("starting point")) {
			return 's';
		}
		if(itemName.toLowerCase().equals("unknown")) {
			return '?';
		}
		return 0;
	}
	
}

package local.bb.studio_projektowe.scene.room.items;

public class Floor implements RoomItem {
	private static final long serialVersionUID = 1L;
	
	Floor(){}

	@Override
	public char getCode() {
		return '-';
	}
}

package local.bb.studio_projektowe.scene.room.items;

public class Wall implements RoomItem {
	private static final long serialVersionUID = 1L;
	
	Wall(){}

	@Override
	public char getCode() {
		return '+';
	}
}

package local.bb.studio_projektowe.scene.room.items;

public class StartingPoint implements RoomItem {
	private static final long serialVersionUID = 1L;
	
	StartingPoint() {}

	@Override
	public char getCode() {
		return 's';
	}

}

package local.bb.studio_projektowe.scene.room.items;

import java.util.Random;

public class Bomb implements RoomItem {
	private static final long serialVersionUID = 1L;
	
	/*
	 * time needed to defude this bomb
	 */
	private final int maxDefuseTime;
	private int defuseTime;
	/*
	 * time left to explode
	 */
	/*
	private int explosionTime;
	private final int maxExplosionTime;
	*/
	private static final Random random = new Random();
	
	public Bomb() {
		this.maxDefuseTime = random.nextInt(5)+20;
		//this.maxExplosionTime = random.nextInt(20)+20;
		this.defuseTime = maxDefuseTime;
		//this.explosionTime = maxExplosionTime;
	}
/*
	public int getExplosionTime() {
		return explosionTime;
	}

	public void tickBomb() {
		--this.explosionTime;
	}
*/
	public int getMaxDefuseTime() {
		return maxDefuseTime;
	}
/*
	public int getMaxExplosionTime() {
		return maxExplosionTime;
	}
*/
	public int getDefuseTime() {
		return defuseTime;
	}
	
	public void defuse() {
		--this.defuseTime;
		//System.out.println("defusing bomb " + this.getDefuseTime());
	}

	@Override
	public char getCode() {
		return 'b';
	}

}

package local.bb.studio_projektowe.scene.room.items;

import local.bb.studio_projektowe.exceptions.RoomCreationException;
import local.bb.studio_projektowe.scene.room.Room;

public class RoomBuilder {
	
	private String currentRoomString = "";
	private Integer cols = null;
	
	public RoomBuilder addRow(String row) throws RoomCreationException {
		if(cols == null) {
			cols = row.length();
		} else if(cols != row.length()) {
			throw new RoomCreationException("New row's length cannot differ from others'");
		}
		currentRoomString += row + Room.separator;
		return this;
	}
	
	public Room build() throws RoomCreationException {
		return new Room(currentRoomString);
	}

}

package local.bb.studio_projektowe.scene.room.items;

import java.io.Serializable;

public interface RoomItem extends Serializable {
	/*
	 * codes:
	 * 		b	bomb
	 * 		+	wall
	 * 		-	floor
	 * 		s	starting point
	 * */
	char getCode();
}

package local.bb.studio_projektowe.scene.room;

import local.bb.studio_projektowe.exceptions.BombDefuseException;
import local.bb.studio_projektowe.exceptions.DirectionException;
import local.bb.studio_projektowe.exceptions.RoomCreationException;
import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.scene.room.Position.DIRECTION;
import local.bb.studio_projektowe.scene.room.items.Bomb;
import local.bb.studio_projektowe.scene.room.items.RoomItem;
import local.bb.studio_projektowe.scene.room.items.RoomItemFactory;

/**
 * 
 * @author bb
 *	opis -> local.bb.studio_projektowe.scene.Room
 */
public class Room {
	
	private RoomItem[][] items;
	public static final char separator = 'n';
	
	public Room(RoomItem[][] items) {
		this.items = items;
	}
	
	public Room(String itemsString) throws RoomCreationException {
		int cols = itemsString.indexOf(""+separator);
		int rows = itemsString.length() - itemsString.replaceAll(separator+"", "").length();
		this.items = new RoomItem[rows][cols];
		for(int i=0,row=0,col=0 ; i<itemsString.length() ; ++i) {
			char item = itemsString.charAt(i);
			if(item == separator) {
				col = 0;
				++row;
				continue;
			}
			//System.out.println("assign ["+row+"]["+col+"] = " + itemsString.charAt(i));
			items[row][col++] = RoomItemFactory.getItem(itemsString.charAt(i));
		}
		validateRoom();
	}
	
	public int getRows() {
		return this.items.length;
	}
	
	public int getCols() {
		if(this.getRows() == 0) {
			return 0;
		}
		return this.items[0].length;
	}

	public RoomItem itemAt(int row, int col) {
		return this.items[row][col];
	}
	
	public RoomItem[][] getItems() {
		return items;
	}

	private void validateRoom() throws RoomCreationException {
		int startingPoints = 0;
		for(int i=0 ; i<getRows() ; ++i) {
			for(int j=0 ; j<getCols() ; ++j) {
				if(itemAt(i,j).getCode() == 's') {
					++startingPoints;
					if(startingPoints > 1) {
						throw new RoomCreationException("there has to be exactly one starting point");
					}
					//checking starting point if there is any way out
					if(i==0 || items[i-1][j].getCode() == RoomItemFactory.getCodeOf("wall")) {
						if(j==0 || items[i][j-1].getCode() == RoomItemFactory.getCodeOf("wall")) {
							if(i==getRows()-1 || items[i+1][j].getCode() == RoomItemFactory.getCodeOf("wall")) {
								if(j==getCols()-1 || items[i][j+1].getCode() == RoomItemFactory.getCodeOf("wall")) {
									throw new RoomCreationException("starting point is trapped");
								}
							}
						}
					}
					//checking starting point if it's not in the middle of the room(there has to be at least one edge next to it)
					if(!(i==0 || j==0 || i==getRows()-1 || j==getCols()-1)) {
						throw new RoomCreationException("there has to be at least one edge next to the starting point");
					}
				}
			}
		}
		//checking if a sample robot is able to explore this room
		try {
			Robot robot = new Robot(null, this, true, 0, null);
			robot.getRoomExplorer().exploreRoom();
			if(!robot.getRoomExplorer().isExplored()) {
				throw new RoomCreationException("the robot will not be able to explore this room");
			}
		} catch (DirectionException | InterruptedException e) {
			e.printStackTrace();
		}
		
		
		if(startingPoints != 1) {
			throw new RoomCreationException("there has to be exactly one starting point");
		}
	}
	
	public void defuseBombAt(int row, int col) throws BombDefuseException {
		if(itemAt(row, col).getCode() != RoomItemFactory.getCodeOf("bomb")) {
			throw new BombDefuseException("trying to defuse the item which is not a bomb");
		}
		Bomb bomb = (Bomb)itemAt(row, col);
		bomb.defuse();
		if(bomb.getDefuseTime() == 0) {
			this.items[row][col] = RoomItemFactory.getItem(RoomItemFactory.getCodeOf("floor"));
		}
	}
	
	public void markBombAsDefused(int row, int col) {
		if(this.items[row][col].getCode() == RoomItemFactory.getCodeOf("bomb")) {
			this.items[row][col] = RoomItemFactory.getItem(RoomItemFactory.getCodeOf("floor"));
		}
	}
	
	/*
	 * returns proper starting direction in the name of the rule that the robot always turns right what means that it has
	 * the wall always on the left hand
	 */
	public DIRECTION getStartingDirection() {
		int i = getStartingPosition().getRow();
		int j = getStartingPosition().getCol();
		if(i==0) return DIRECTION.RIGHT;
		if(j==0) return DIRECTION.UP;
		if(i==getRows()-1) return DIRECTION.LEFT;
		if(j==getCols()-1) return DIRECTION.BOTTOM;
		return null;
	}
	
	public Position getStartingPosition() {
		for(int i=0 ; i<getRows() ; ++i) {
			for(int j=0 ; j<getCols() ; ++j) {
				if(itemAt(i,j).getCode() == 's') {
					return new Position(i, j);
				}
			}
		}
		return null;
	}
	
	public static String getItemsString(RoomItem[][] items) {
		String res = "";
		for(int i=0;i<items.length;++i) {
			for(int j=0;j<items[0].length;++j) {
				res += items[i][j].getCode();
			}
			res += "\n";
		}
		return res;
	}
	
	
}

package local.bb.studio_projektowe.scene.room;

import java.io.Serializable;

public class Position implements Serializable {

	private static final long serialVersionUID = 1L;
	public enum DIRECTION {
		UP,
		RIGHT,
		BOTTOM,
		LEFT
	}
	
	private int row;
	private int col;
	private DIRECTION direction;
	
	public Position(int x, int y, DIRECTION direction) {
		this.row = x;
		this.col = y;
		this.direction = direction;
	}
	
	public Position(int x, int y) {
		this.row = x;
		this.col = y;
		this.direction = null;
	}
	
	public double distance(Position p) {
		return Math.sqrt((this.row-p.getRow())*(this.row-p.getRow()) + (this.col-p.getCol())*(this.col-p.getCol()));
	}
	
	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public DIRECTION getDirection() {
		return direction;
	}
	public void setDirection(DIRECTION direction) {
		this.direction = direction;
	}
	
	@Override
	public String toString() {
		return "Position [row=" + row + ", col=" + col + ", direction=" + direction + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + row;
		result = prime * result + col;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (row != other.row)
			return false;
		if (col != other.col)
			return false;
		return true;
	}
	
	
	
}

package local.bb.studio_projektowe.tools.communication;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonSyntaxException;

import local.bb.studio_projektowe.interfaces.AgentInterface;
import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.scene.room.Position;
import local.bb.studio_projektowe.scene.room.items.Bomb;
import local.bb.studio_projektowe.scene.room.items.RoomItem;
import local.bb.studio_projektowe.scene.room.items.RoomItemFactory;
import local.bb.studio_projektowe.tools.RobotPersisted;
import local.bb.studio_projektowe.tools.communication.Message.SUBJECTS;
import local.bb.studio_projektowe.tools.communication.Message.TYPES;

public class Receiver implements Runnable {
	
	private final AgentInterface robotAgent;
	private final Map<String, BombRequestTask> bombRequestTasks = new HashMap<>();
	
	public Receiver(AgentInterface robotAgent) {
		this.robotAgent = robotAgent;
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public void run() {
		//System.out.println("receiver start for " + robotAgent);
		while(true) {
			String messageStr = robotAgent.rcv();
			Message message = null;
			try {
				message = CommunicationManager.deserialize(messageStr, Message.class);
			} catch (JsonSyntaxException e) {
				robotAgent.log("json syntax exception unexpected string: " + messageStr);
			}
			//System.out.println("RECEIVED: " + message.toString());
			robotAgent.log("RECEIVED: " + message.toString());
			if(message.getType() == TYPES.REQUEST) {
				String response = null;
				Position pos = null;
				switch(message.getSubject()) {
				case HANDLE_BOMB:
					pos = CommunicationManager.deserialize(message.getContent(), Position.class);
					robotAgent.log("got request HANdlE BOMB");
					RoomItem item = robotAgent.getRobot().getItems()[pos.getRow()][pos.getCol()];
					if(item.getCode() == RoomItemFactory.getCodeOf("floor") ||
							(item.getCode() == RoomItemFactory.getCodeOf("bomb") &&
							robotAgent.getRobot().getBombToDefuse() != null &&
							robotAgent.getRobot().getBombToDefuse() == (Bomb)item)) {
						//send that it has been defused/taken care of
						response = "handled";
					} else if(robotAgent.getRobot().getBombToDefuse() == null && 
							robotAgent.getRobot().getCurrentItem().getCode() != RoomItemFactory.getCodeOf("bomb") &&
							robotAgent.getRobot().getItems()[pos.getRow()][pos.getCol()].getCode() ==
							RoomItemFactory.getCodeOf("unknown")) {
						//send that this robot can help
						response = CommunicationManager.serialize(robotAgent.getRobot().getRobotPersisted());
						
					} else {
						//send that this robot is busy
						response = "no";
					}
					robotAgent.sendToOne(
							new Message(
									TYPES.RESPONSE, 
									SUBJECTS.HANDLE_BOMB, 
									response,
									robotAgent.getAgentName()), 
							message.getAuthor());
					robotAgent.log("sended response HANDLE BOBM " + response);
					break;
				/*case BUSY:
					response = 
							(robotAgent.getRobot().getBombToDefuse() == null && 
							robotAgent.getRobot().getCurrentItem().getCode() != RoomItemFactory.getCodeOf("bomb")) ? 
									"no" : 
									"yes";
					robotAgent.log("sending response to BUSY: " + response);
					robotAgent.sendToOne(
							new Message(
									TYPES.RESPONSE, 
									SUBJECTS.BUSY, 
									response,
									robotAgent.getAgentName()), 
							message.getAuthor());
					break;
				case HELP:
					pos = CommunicationManager.deserialize(message.getContent(), Position.class);
					response = 
							(robotAgent.getRobot().getBombToDefuse() == null &&
							robotAgent.getRobot().getItems()[pos.getRow()][pos.getCol()].getCode() ==
								RoomItemFactory.getCodeOf("unknown")) ? 
										CommunicationManager.serialize(robotAgent) : 
										"no";
					robotAgent.log("sending response to HELP: " + response);
					robotAgent.sendToOne(
							new Message(
									TYPES.RESPONSE, 
									SUBJECTS.HELP, 
									response,
									robotAgent.getAgentName()), 
							message.getAuthor());
					break;*/
				case ORDER:
					Position positionReceived = CommunicationManager.deserialize(message.getContent(), Position.class);
					Robot robot = robotAgent.getRobot();
					robot.setItemFromRoom(positionReceived.getRow(), positionReceived.getCol());
					Bomb bomb = (Bomb)robot.getItems()[positionReceived.getRow()][positionReceived.getCol()];
					robot.setBombToDefuse(bomb);
					break;
					/*
				case CONFIRM:
						pos = CommunicationManager.deserialize(message.getContent(), Position.class);
						Robot r = robotAgent.getRobot();
						r.setItemFromRoom(pos.getRow(), pos.getCol());
						r.setBombToDefuse((Bomb)r.getItems()[pos.getRow()][pos.getCol()]);
					break;
					*/
				}
			} else if(message.getType() == TYPES.RESPONSE) {
				String partner = message.getAuthor();
				BombRequestTask task = bombRequestTasks.get(partner);
				if(task != null) {
					task.setResponse(message);
				} else {
					System.err.println("received message from non connected agent");
				}
			}
		}
	}
	
	public BombRequestTask addTask(String partner, Position bombPosition) {
		/*if(bombRequestTasks.containsKey(partner)) {
			return null;
		}*/
		BombRequestTask task = new BombRequestTask(robotAgent.getAgentName(), this, partner, bombPosition);
		bombRequestTasks.put(partner, task);
		return task;
	}
	
	public static class BombRequestTask {
		
		private final String owner;
		private final Receiver receiver;
		private Message response = null;
		private String partner;
		private final Position bombPosition;
		
		public BombRequestTask(String owner, Receiver receiver, String partner, Position bombPosition) {
			this.owner = owner;
			this.receiver = receiver;
			this.partner = partner;
			this.bombPosition = bombPosition;
		}

		/*
		 * RECEIVE		SEND
		 * 				BUSY?
		 * yes/no
		 * 				if no -> HELP(position)?
		 * yes/no [yes if the partner haven't explored bomb by now, no if they have]

		 * 				if yes 	-> choose one of the partners who said yes and send them CONFIRM(position)
		 * 				if no 	-> send DECLINE to others
		 */
		public RobotPersisted proceed() {
			try {
				receiver.robotAgent.log(this.toString());
				receiver.robotAgent.sendToOne(
						new Message(
								TYPES.REQUEST, 
								SUBJECTS.HANDLE_BOMB, 
								CommunicationManager.serialize(bombPosition), 
								owner),
						partner);
				receiver.robotAgent.log("sended HANDLE_BOMB request, waiting for response...");
				waitForResponse(SUBJECTS.HANDLE_BOMB);
				String responseStr = response.getContent();
				receiver.robotAgent.log("got response HANDLE_BOMB " + responseStr);
				if(responseStr.equals("handled")) {
					//the bomb has been defused/taken care of
					receiver.robotAgent.getRobot().markBombAsDefused(bombPosition.getRow(), bombPosition.getCol());
					receiver.robotAgent.log("bomb marked as defused " + bombPosition.toString());
				} else if(responseStr.equals("no")) {
					//this robot is busy
					return null;
				} else {
					//this robot can help
					RobotPersisted helper = CommunicationManager.deserialize(responseStr, RobotPersisted.class);
					return helper;
				}
				/*
				receiver.robotAgent.sendToOne(new Message(TYPES.REQUEST, SUBJECTS.BUSY, null, owner), partner);
				receiver.robotAgent.log("sended BUSY request, waiting for response...");
				waitForResponse(SUBJECTS.BUSY);
				if(!response.getContent().equals("no")) {
					//System.out.println(partner + " is busy");
					receiver.robotAgent.log(partner + " is busy");
					return null;
				}
				receiver.robotAgent.sendToOne(
						new Message(
							TYPES.REQUEST, 
							SUBJECTS.HELP, 
							CommunicationManager.serialize(bombPosition), 
							owner), 
						partner);
				receiver.robotAgent.log("sended HELP request, waiting for response...");
				waitForResponse(SUBJECTS.HELP);
				if(!response.getContent().equals("no")) {
					receiver.robotAgent.log("found helper");
					return CommunicationManager.deserialize(response.getContent(), AgentInterface.class);
				}*/
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			receiver.robotAgent.log("didn't find helper");
			return null;
		}
		
		public synchronized Message getResponse() {
			return response;
		}

		public synchronized void setResponse(Message reponse) {
			this.response = reponse;
			if(this.receiver != null) {
				this.notifyAll();
			}
		}

		public synchronized void waitForResponse(SUBJECTS subject) throws InterruptedException {
			while(response == null || response.getSubject() != subject) {
				if(response != null) {
					response = null;
				}
				wait();
			}
		}

		@Override
		public String toString() {
			return "BombRequestTask [owner=" + owner + ", partner=" + partner + "]";
		}
		
		
		
	}

	public AgentInterface getRobotAgent() {
		return robotAgent;
	}
	
	
	
	/*
	public class BombDetectedTask implements Runnable {

		private final String partner;
		private final Position bombPosition;
		
		public BombDetectedTask(String partner, Position bombPosition) {
			this.partner = partner;
			this.bombPosition = bombPosition;
		}

		@Override
		public void run() {
			robotAgent.send(
					new Message(
							TYPES.REQUEST,
							SUBJECTS.BOMB_FOUND,
							null,
							robotAgent.getAgentName()),
					partner);
			
		}
		
	}
	*/
}
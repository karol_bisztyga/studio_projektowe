package local.bb.studio_projektowe.tools.communication;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import local.bb.studio_projektowe.interfaces.AgentInterface;
import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.scene.room.Position;
import local.bb.studio_projektowe.tools.RobotPersisted;
import local.bb.studio_projektowe.tools.communication.Message.SUBJECTS;
import local.bb.studio_projektowe.tools.communication.Message.TYPES;
import local.bb.studio_projektowe.tools.communication.Receiver.BombRequestTask;

/**
 * 
 * algorytm decydowania o rozbrajaniu bomb:
 * robot, ktory napotyka bombe pyta sie reszty robotow, czy ktorys jej nie rozbroi, tak, �eby ten, ktory ja odkryl
 * mogl eksplorowac dalej. Kazdy inny robot, ktory nie jest zajety i jeszcze nie zobaczyl tej bomby ma sie zglosic.
 * Eksplorujacy robot wysyla potwierdzenie do robota, ktory jest polozony najdalej od niego. 
 * Wtedy tamten dojezdza do bomby i ja rozbraja. Jezeli wszystkie inne roboty sa zajete, eksplorujacy rozbraja bombe itp itd.
 * 
 */
public class CommunicationManager {
	
	public static final int RESPONSE_TIMEOUT_SECONDS = 5;
	
	private final AgentInterface robotAgent;
	//private final Sender sender;
	private final Receiver receiver;
	
	public CommunicationManager(AgentInterface robotAgent) {
		this.robotAgent = robotAgent;
		//sender = new Sender(robotAgent);
		receiver = new Receiver(robotAgent);
	}
	
	public void start() {
		//Thread senderThread = new Thread(sender);
		Thread receiverThread = new Thread(receiver);
		
		//senderThread.start();
		receiverThread.start();
	}
	
	private static Gson gson = new Gson();
	
	public static String serialize(Object obj) {
		return gson.toJson(obj);
	}
	
	public static <T extends Object> T deserialize(String str, Class<T> type) {
		return gson.fromJson(str, type);
	}
	
	public Receiver getReceiver() {
		return receiver;
	}

	public AgentInterface getRobotAgent() {
		return robotAgent;
	}
	
	public Callable<Boolean> isAnybodyGoingToHandleThisBomb(final Position bombPosition) {
		return new Callable<Boolean>() {
			@Override
			public Boolean call() throws InterruptedException {
				ArrayList<String> partners = getRobotAgent().getAgentsList();
				partners.remove(getRobotAgent().getAgentName());
				//check if there are any other robots
				if(partners.size() == 0) {
					return false;
				}
				robotAgent.log("looking for helpers among " + partners.size() + " robot(s) with " + bombPosition);
				ExecutorService ex = Executors.newCachedThreadPool();
				List<FutureTask<RobotPersisted>> tasks = new ArrayList<>();
				for(int i=0; i<partners.size() ; ++i) {
					FutureTask<RobotPersisted> ft = 
							new FutureTask<>(isThisAgentGoingToHandleThisBomb(partners.get(i), bombPosition));
					tasks.add(ft);
					ex.execute(ft);
				}
				ex.shutdown();
				ex.awaitTermination(RESPONSE_TIMEOUT_SECONDS, TimeUnit.SECONDS);
				List<RobotPersisted> possibleHelpers = new ArrayList<>();
				for(int i=0 ; i<tasks.size() ; ++i) {
					RobotPersisted result;
					try {
						result = tasks.get(i).get();
						if(result != null) {
							possibleHelpers.add(result);
						}
					} catch (ExecutionException e) {
					}
				}
				if(possibleHelpers.size() == 0) {
					return false;
				}
				//send order to the furthest robot
				robotAgent.sendToOne(
						new Message(
								TYPES.REQUEST, 
								SUBJECTS.ORDER, 
								CommunicationManager.serialize(bombPosition), 
								robotAgent.getAgentName()), 
						getFurthestAgent(possibleHelpers).getId());
				return true;
			}
		};
	}
	
	public Callable<RobotPersisted> isThisAgentGoingToHandleThisBomb(final String agentName, final Position bombPosition) {
		return new Callable<RobotPersisted>() {
			@Override
			public RobotPersisted call() throws Exception {
				BombRequestTask task = receiver.addTask(agentName, bombPosition);
				if(task != null) {
					return task.proceed();
				}
				return null;
			}
		};
	}
	
	private RobotPersisted getFurthestAgent(List<RobotPersisted> robots) {
		RobotPersisted result = robots.get(0);
		double resultDist = result.getPosition().distance(this.robotAgent.getRobot().getPosition());
		for(int i=1 ; i<robots.size() ; ++i) {
			double dist = robots.get(i).getPosition().distance(this.robotAgent.getRobot().getPosition());
			if(dist > resultDist) {
				result = robots.get(i);
				resultDist = dist;
			}
		}
		return result;
	}
	
}

package local.bb.studio_projektowe.tools.communication;

public class Message {
	public enum TYPES {
		REQUEST,
		RESPONSE
	}
	
	public enum SUBJECTS {
		INFO,
		HANDLE_BOMB,
		ORDER
	}
	
	private final String author;
	private final TYPES type;
	private final SUBJECTS subject;
	private final String content;
	
	public Message(TYPES type, SUBJECTS subject, String content, String author) {
		this.type = type;
		this.subject = subject;
		this.content = content;
		this.author = author;
	}
	
	public TYPES getType() {
		return type;
	}
	public SUBJECTS getSubject() {
		return subject;
	}
	public String getContent() {
		return content;
	}
	public String getAuthor() {
		return author;
	}

	@Override
	public String toString() {
		return "Message [author=" + author + ", type=" + type + ", subject=" + subject + ", content=" + content + "]";
	}
	
}

package local.bb.studio_projektowe.tools;

import java.io.Serializable;

import local.bb.studio_projektowe.scene.room.Position;

public class RobotPersisted implements Serializable {
	
	private static final long serialVersionUID = 1L;
	//private static final Gson gson = new Gson();
	private final String id;
	private Position position;
	
	public RobotPersisted(String id, Position position) {
		this.id = id;
		this.position = position;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RobotPersisted other = (RobotPersisted) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
/*
	public static String serialize(RobotPersisted robot) {
		return gson.toJson(robot);
	}
	
	public static RobotPersisted deserialize(String str) {
		return gson.fromJson(str, RobotPersisted.class);
	}
	*/
}

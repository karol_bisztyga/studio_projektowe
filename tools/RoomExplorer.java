package local.bb.studio_projektowe.tools;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import local.bb.studio_projektowe.SPMain;
import local.bb.studio_projektowe.exceptions.BombDefuseException;
import local.bb.studio_projektowe.exceptions.DirectionException;
import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.scene.room.Position;
import local.bb.studio_projektowe.scene.room.Position.DIRECTION;
import local.bb.studio_projektowe.scene.room.items.Bomb;
import local.bb.studio_projektowe.scene.room.items.RoomItem;
import local.bb.studio_projektowe.scene.room.items.RoomItemFactory;
import local.bb.studio_projektowe.tools.communication.CommunicationManager;

public class RoomExplorer {
	public enum TURN_OPTION {
		LEFT,
		RIGHT
	}
	
	public enum SIDE {
		FRONT,
		RIGHT,
		BACK,
		LEFT
	}
	
	private final Robot robot;
	private boolean justTurnedLeft = false;
	private boolean robotMoved = false;
	private final boolean dummy;
	private int offset;
	private boolean changed = false;
	private final CommunicationManager communicationManager;
	
	public RoomExplorer(Robot robot, boolean dummy, CommunicationManager communicationManager) {
		this(robot, dummy, 0, communicationManager);
	}
	
	public RoomExplorer(Robot robot, boolean dummy, int offset, CommunicationManager communicationManager) {
		this.robot = robot;
		this.dummy = dummy;
		this.offset = offset;
		this.communicationManager = communicationManager;
	}
	
	private DIRECTION getTurnDirection(TURN_OPTION turnOption) throws DirectionException {
		switch(robot.getPosition().getDirection()) {
			case UP:
				if(turnOption == TURN_OPTION.RIGHT) {
					return DIRECTION.RIGHT;
				} else {
					return DIRECTION.LEFT;
				}
			case RIGHT:
				if(turnOption == TURN_OPTION.RIGHT) {
					return DIRECTION.BOTTOM;
				} else {
					return DIRECTION.UP;
				}
			case BOTTOM:
				if(turnOption == TURN_OPTION.RIGHT) {
					return DIRECTION.LEFT;
				} else {
					return DIRECTION.RIGHT;
				}
			case LEFT:
				if(turnOption == TURN_OPTION.RIGHT) {
					return DIRECTION.UP;
				} else {
					return DIRECTION.BOTTOM;
				}
		}
		throw new DirectionException();
	}
	
	private void turn(TURN_OPTION turnOption) throws DirectionException {
		robot.getPosition().setDirection(getTurnDirection(turnOption));
	}
	
	private boolean hasWallOn(SIDE side) {
		Position pos = robot.getPosition();
		LazyBooleanCallBack hasWallAbove = new LazyBooleanCallBack((pos.getRow()==0 ||
				robot.getRoom().itemAt(pos.getRow()-1, pos.getCol()).getCode() == RoomItemFactory.getCodeOf("wall")),
				robot,
				pos.getRow()-1,
				pos.getCol());
		LazyBooleanCallBack hasWallOnRight = new LazyBooleanCallBack((pos.getCol()==robot.getRoom().getCols()-1 || 
				robot.getRoom().itemAt(pos.getRow(), pos.getCol()+1).getCode() == RoomItemFactory.getCodeOf("wall")),
				robot, 
				pos.getRow(), 
				pos.getCol()+1);
		LazyBooleanCallBack hasWallBelow = new LazyBooleanCallBack((pos.getRow()==robot.getRoom().getRows()-1 || 
				robot.getRoom().itemAt(pos.getRow()+1, pos.getCol()).getCode() == RoomItemFactory.getCodeOf("wall")), 
				robot, 
				pos.getRow()+1, 
				pos.getCol());
		LazyBooleanCallBack hasWallOnLeft = new LazyBooleanCallBack((pos.getCol()==0 || 
				robot.getRoom().itemAt(pos.getRow(), pos.getCol()-1).getCode() == RoomItemFactory.getCodeOf("wall")), 
				robot,
				pos.getRow(),
				pos.getCol()-1);
		
		switch(pos.getDirection()) {
			case UP:
				switch(side) {
					case FRONT:
						return hasWallAbove.getValue();
					case RIGHT:
						return hasWallOnRight.getValue();
					case BACK:
						return hasWallBelow.getValue();
					case LEFT:
						return hasWallOnLeft.getValue();
				}
				break;
			case RIGHT:
				switch(side) {
					case FRONT:
						return hasWallOnRight.getValue();
					case RIGHT:
						return hasWallBelow.getValue();
					case BACK:
						return hasWallOnLeft.getValue();
					case LEFT:
						return hasWallAbove.getValue();
				}
				break;
			case BOTTOM:
				switch(side) {
					case FRONT:
						return hasWallBelow.getValue();
					case RIGHT:
						return hasWallOnLeft.getValue();
					case BACK:
						return hasWallAbove.getValue();
					case LEFT:
						return hasWallOnRight.getValue();
				}
				break;
			case LEFT:
				switch(side) {
					case FRONT:
						return hasWallOnLeft.getValue();
					case RIGHT:
						return hasWallAbove.getValue();
					case BACK:
						return hasWallOnRight.getValue();
					case LEFT:
						return hasWallBelow.getValue();
				}
				break;
		}
		return false;
	}
	
	private static class LazyBooleanCallBack {
		private boolean value;
		private Robot robot;
		private int row;
		private int col;
		
		public LazyBooleanCallBack(boolean value, Robot robot, int row, int col) {
			this.value = value;
			this.robot = robot;
			this.row = row;
			this.col = col;
		}
		private boolean exploreWall() {
			try {
				robot.setItem(row, col, robot.getRoom().itemAt(row, col));
				return true;
			} catch (ArrayIndexOutOfBoundsException e) {
				return false;
			}
		}
		public boolean getValue() {
			return getValue(true);
		}
		public boolean getValue(boolean callCallback) {
			if(value && callCallback) {
				exploreWall();
			}
			return value;
		}
	}
	
	public void moveForward() {
		Position pos = robot.getPosition();
		switch(pos.getDirection()) {
			case UP:
				robot.getPosition().setRow(pos.getRow()-1);
				break;
			case RIGHT:
				robot.getPosition().setCol(pos.getCol()+1);
				break;
			case BOTTOM:
				robot.getPosition().setRow(pos.getRow()+1);
				break;
			case LEFT:
				robot.getPosition().setCol(pos.getCol()-1);
				break;
		}
		justTurnedLeft = false;
		robotMoved = true;
	}
	
	/**
	 * algorytm:
	 * ustaw sie tak, zeby miec sciane po lewej stronie.
	 * idz do przodu tak, aby miec sciane po lewej stronie
	 * jesli w kolejnym kroku nie bedzie sciany po lewej, skrec w lewo
	 * jesli napotkasz na sciane z przodu, skrec w prawo
	 */
	public void exploreRoom() throws DirectionException, InterruptedException {
		if(communicationManager != null) {
			communicationManager.start();
		}
		ExecutorService ex = null;
		while(true) {
			if(offset <= 0) {
				Position pos = robot.getPosition();
				RoomItem currentItem = robot.getRoom().itemAt(pos.getRow(), pos.getCol());
				boolean defusing = false;
				if(!dummy && currentItem.getCode() == RoomItemFactory.getCodeOf("bomb")) {
					Bomb bomb = (Bomb)currentItem;
					communicationManager.getReceiver().getRobotAgent().log("--explorer found bomb! " + robot.getBombToDefuse());
					//see the bomb for a first time
					if(robot.getBombToDefuse() == null) {
						try {
							Callable<Boolean> c = communicationManager.isAnybodyGoingToHandleThisBomb(pos);
							if(ex != null) {
								ex.shutdownNow();
							}
							ex = Executors.newCachedThreadPool();
							Future<Boolean> f = ex.submit(c);
							ex.shutdown();
							//wait for response
							ex.awaitTermination(CommunicationManager.RESPONSE_TIMEOUT_SECONDS, TimeUnit.SECONDS);
							if(!f.isDone()) {
								//if there's no answer, you have to do it
								defusing = true;
							} else {
								//if nooene else is going to handle this bomb, you have to do it
								//if someone declared their will to do it, go further
								defusing = !f.get();
							}
							ex.shutdownNow();
						} catch (ExecutionException e) {
							//communication failure, you have to deal with this bomb
							defusing = true;
						}
						if(defusing) {
							robot.setBombToDefuse(bomb);
						}
					}
					if(robot.getBombToDefuse() == bomb) {
						defusing = true;
						try {
							robot.getRoom().defuseBombAt(pos.getRow(), pos.getCol());
							robot.setItem(pos.getRow(), pos.getCol(), 
									robot.getRoom().itemAt(pos.getRow(), pos.getCol()));
							
							//bomb defused
							if(bomb.getDefuseTime() == 0) {
								robot.setBombToDefuse(null);
							}
						} catch (BombDefuseException e) {
							defusing = false;
							if(ex != null) {
								ex.shutdownNow();
							}
						}
					}
				}
				if(!defusing) {
					robot.setBombToDefuse(null);
					if(!hasWallOn(SIDE.LEFT) && !justTurnedLeft) {
						turn(TURN_OPTION.LEFT);
						justTurnedLeft = true;
					} else if(hasWallOn(SIDE.FRONT)) {
						turn(TURN_OPTION.RIGHT);
					} else {
						moveForward();
						robot.setItem(pos.getRow(), pos.getCol(), robot.getRoom().itemAt(pos.getRow(), pos.getCol()));
					}
					if(robotMoved && robot.getPosition().equals(robot.getRoom().getStartingPosition())) {
						break;
					}
				}
			} else {
				--offset;
			}
			setChanged(true);
			if(!dummy) {
				TimeUnit.MILLISECONDS.sleep(SPMain.TIMEOUT);
			}
		}
	}
	
	public synchronized void waitForChange() throws InterruptedException {
		while(!isChanged()) {
			this.wait();
		}
	}
	
	public synchronized boolean isChanged() {
		return changed;
	}

	public synchronized void setChanged(boolean changed) {
		this.changed = changed;
		if(changed) {
			this.notifyAll();
		}
	}
	
	public boolean isExplored() {
		for(int i=0 ; i<robot.getRoom().getRows();  ++i) {
			for(int j=0 ; j<robot.getRoom().getCols() ; ++j) {
				if(robot.getItems()[i][j].getCode() == RoomItemFactory.getCodeOf("unknown")) {
					return false;
				}
			}
		}
		return true;
	}

	public double percentageExplored() {
		double points = 0;
		for(int i=0 ; i<robot.getRoom().getRows();  ++i) {
			for(int j=0 ; j<robot.getRoom().getCols() ; ++j) {
				if(robot.getItems()[i][j].getCode() != RoomItemFactory.getCodeOf("unknown")) {
					++points;
				}
			}
		}
		return points/(robot.getCols()*robot.getRows())*100;
	}
	
}

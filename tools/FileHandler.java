package local.bb.studio_projektowe.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import local.bb.studio_projektowe.exceptions.RoomCreationException;
import local.bb.studio_projektowe.scene.room.Room;
import local.bb.studio_projektowe.scene.room.items.RoomBuilder;

public class FileHandler {
/*
	private final static String DIR_NAME = "agents";
	
	public FileHandler() {
		new File(DIR_NAME).mkdirs();
	}
	
	public void registerAgent(String id) throws IOException {
		File file = new File(DIR_NAME + File.separator + id);
		file.createNewFile();
	}
	
	public void removeAgent(String id) {
		File file = new File(DIR_NAME + File.separator + id);
		file.delete();
	}
	
	public ArrayList<String> getAgents() {
		ArrayList<String> result = new ArrayList<>();
		File dir = new File(DIR_NAME);
		File[] files = dir.listFiles();
		for(File f : files) {
			result.add(f.getName());
		}
		return result;
	}
	*/
	public static Room readRoomFromFile(String roomName) 
			throws FileNotFoundException, IOException, RoomCreationException {
		RoomBuilder roomBuilder = new RoomBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader("rooms" + File.separator + roomName))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       roomBuilder.addRow(line);
		    }
		}
		return roomBuilder.build();
	}
	
}

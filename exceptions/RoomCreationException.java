package local.bb.studio_projektowe.exceptions;

public class RoomCreationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public RoomCreationException(String s) {
		super(s);
	}

}

package local.bb.studio_projektowe.exceptions;

public class MoveException extends Exception {
	private static final long serialVersionUID = 1L;

	public MoveException(String s) {
		super(s);
	}
}

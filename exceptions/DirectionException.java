package local.bb.studio_projektowe.exceptions;

public class DirectionException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public DirectionException() {
		this("");
	}
	
	public DirectionException(String s) {
		super(s);
	}

}

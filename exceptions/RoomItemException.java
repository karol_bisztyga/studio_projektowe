package local.bb.studio_projektowe.exceptions;

public class RoomItemException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public RoomItemException(String s) {
		super(s);
	}

}

package local.bb.studio_projektowe.exceptions;

public class BombDefuseException extends Exception {
	private static final long serialVersionUID = 1L;

	public BombDefuseException(String s) {
		super(s);
	}
}

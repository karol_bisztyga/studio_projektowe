package local.bb.studio_projektowe.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class Test {
	/*
	public static void main(String[] args) {
		try {
			RoomBuilder roomBuilder = new RoomBuilder();
			*/
			/*
			Room room = roomBuilder
					.addRow("-++")
					.addRow("--+")
					.addRow("--s")
					.build();
			*/
/*
			Room room = roomBuilder
					.addRow("+-+")
					.addRow("---")
					.addRow("+--")
					.addRow("+s-")
					.build();
			*/
/*
			Room room = roomBuilder
					.addRow("--------+-----s")
					.addRow("+--+---+---+---")
					.addRow("---+---b---+---")
					.addRow("---++---+---+--")
					.addRow("---+-+----+----")
					.addRow("---+----b--+---")
					.addRow("++----++----+--")
					.addRow("--b+-+-----++--")
					.addRow("---------------")
					.addRow("---+++++--++---")
					.addRow("---+---+-------")
					.addRow("---+-----------")
					.build();
			*/
			
			/*
			Room room = roomBuilder
					.addRow("+----+")
					.addRow("--+---")
					.addRow("--++--")
					.addRow("+-b--+")
					.addRow("+----s")
					.build();
			*
			Room room = roomBuilder
					.addRow("----------------+---+---+---+---")
					.addRow("-------------++---+---+---+---+-")
					.addRow("-+++++++++++-++++++++++++++++++-")
					.addRow("-+-+-+--+--+-+---+---+---+---+--")
					.addRow("-+-+-+--+--+-+-+-+-+-+-+-+-+-+--")
					.addRow("s+-------------+---+---+---+-+--")
					.build();
			
			System.out.println(room.toString());
			
			Robot robot = new Robot("robot", room);

			robot.getRoomExplorer().exploreRoom();
			print(robot, "");
			/*
			int i=0;
			while(true) {
				robot.getRoomExplorer().exploreRoom();
				print(robot, "explored ["+ i +"]");
				if(robot.getRoomExplorer().isExplored()) break;
				robot.getRoomExplorer().convertTiles();
				print(robot, "converted ["+ i +"]");
				++i;
			}
			print(robot, "final");
			*/
		/*} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}*/
/*
			System.out.println(Room.getItemsString(robot.getItems()));
			System.out.println(robot.getPosition().getDirection());
			
			try {
				robot.getRoomExplorer().exploreRoom();
			} catch (DirectionException e) {
				System.out.println("could not explore room: " + e.getMessage());
			}
			System.out.println(Room.getItemsString(robot.getItems()));
			robot.getRoomExplorer().convertTiles();
			System.out.println(Room.getItemsString(robot.getItems()));
			System.out.println(robot.getPosition().getDirection());
			
		} catch (RoomCreationException e) {
			System.out.println("could not create room: " + e.getMessage());
		} catch (DirectionException e) {
			e.printStackTrace();
		}
		*/
	/*
	}
	
	public static void print(Robot robot, String s) {
		System.out.println("==============    "+ s.toUpperCase() +"    =============");
		System.out.println("ROOM: ");
		System.out.println(Room.getItemsString(robot.getRoom().getItems()));
		System.out.println("ROBOT: ");
		System.out.println(Room.getItemsString(robot.getItems()));
		System.out.println("==============================================================================");
	}*/
	

	public static Callable<Boolean> getResult(final int n) {
		return new Callable<Boolean>() {
			@Override
			public Boolean call() throws Exception {
				//boolean results[] = new boolean[n];
				ExecutorService ex = Executors.newCachedThreadPool();
				List<FutureTask<Boolean>> tasks = new ArrayList<>();
				for(int i=0;i<n;++i) {
					System.out.println("loop " + i);
					FutureTask<Boolean> ft = new FutureTask<>(c(i));
					tasks.add(ft);
					ex.execute(ft);
				}
				ex.shutdown();
				ex.awaitTermination(5, TimeUnit.SECONDS);
				for(int i=0;i<n;++i) {
					//if(results[i]) return true;
					Boolean b = tasks.get(i).get();
					System.out.println(i + " -> " + b);
					if(b) {
						return true;
					}
				}
				return false;
				/*for(int i=0;i<n;++i) {
					Future<Boolean> f = ex.submit(c(i));
					results[i] = f.get();
				}
			}*/
			}
		};
	}
	
	public static Callable<Boolean> c(final int n) {
		return new Callable<Boolean>() {
			@Override
			public Boolean call() throws Exception {
				Thread.sleep(n*500);
				return ((n+1)%3==0);
			}
		};
	}
	
	public static void main(String[] args) {
		/*
		try {
			System.out.println("here");
			Callable<Boolean> c = getResult(5);
			ExecutorService ex = Executors.newCachedThreadPool();
			Future<Boolean> f = ex.submit(c);
			ex.shutdown();
			ex.awaitTermination(5, TimeUnit.SECONDS);
			System.out.println("yo " + f.isDone());
			Boolean b = f.get();
			System.out.println(b);
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		try {
			FileWriter f = new FileWriter(new File("siema.txt"), false);
			f.write("siemka hehe" + System.getProperty("line.separator"));
			f.write("siemka");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
}

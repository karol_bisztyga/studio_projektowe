package local.bb.studio_projektowe.interfaces;

import java.util.ArrayList;
import java.util.List;

import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.tools.communication.Message;

public interface AgentInterface {
	public String rcv();
	public String getAgentName();
	public ArrayList<String> getAgentsList();
	public Robot getRobot();
	public List<String> sendToAllButMe(Message message);
	public List<String> sendToAll(Message message);
	public void sendToOne(Message message, String receiver);
	public List<String> sendToMany(Message message, List<String> receivers);
	public void log(String message);
}

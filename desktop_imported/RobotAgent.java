package local.bb.studio_projektowe.desktop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.AMSService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;

import local.bb.studio_projektowe.SPMain;
import local.bb.studio_projektowe.exceptions.BombDefuseException;
import local.bb.studio_projektowe.exceptions.DirectionException;
import local.bb.studio_projektowe.exceptions.RoomCreationException;
import local.bb.studio_projektowe.interfaces.AgentInterface;
import local.bb.studio_projektowe.scene.Robot;
import local.bb.studio_projektowe.scene.room.Room;
import local.bb.studio_projektowe.tools.FileHandler;
import local.bb.studio_projektowe.tools.communication.CommunicationManager;
import local.bb.studio_projektowe.tools.communication.Message;

public class RobotAgent extends Agent implements AgentInterface {
	private static final long serialVersionUID = 1L;
	
	private final static String idMustContain = "RobotAgent";
	
	private Robot robot;
	private SPMain spMain;
	private LogHandler logger;
	
	@Override
	protected void setup() {
		if(!this.getLocalName().contains(idMustContain)) {
			System.err.println("agent id must contain '"+ idMustContain +"'");
			
			return;
		}
		try {
			Room room = FileHandler.readRoomFromFile("testroom.txt");
			robot = new Robot(this.getLocalName(), room, new CommunicationManager(this));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (RoomCreationException e1) {
			e1.printStackTrace();
		}
		logger = new LogHandler();
		spMain = new SPMain(robot);
		new Thread(new Runnable() {
			@Override
			public void run() {
				LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
				config.width = 800;
				config.height = 600;
				config.resizable = false;
				new LwjglApplication(spMain, config);
			}
		}).start();
		
		try {
			robot.getRoomExplorer().exploreRoom();
		} catch (DirectionException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void doDelete() {
		logger.delete();
		super.doDelete();
	}



	public String rcv() {
		ACLMessage aclmsg = this.blockingReceive();
		return aclmsg.getContent();
	}

	public String getAgentName() {
		return this.getLocalName();
	}
	
	public List<String> sendToAllButMe(Message message) {
		List<String> result = new ArrayList<>();
		ACLMessage aclMsg = new ACLMessage(ACLMessage.INFORM);
    	for(String id : getAgentsList()) {
    		if(id.equals(this.getLocalName())) {
    			continue;
    		}
    		aclMsg.addReceiver(new AID(id, AID.ISLOCALNAME));
    		result.add(id);
    	}
    	aclMsg.setContent(CommunicationManager.serialize(message));
    	send(aclMsg);
    	return result;
	}
	
	public List<String> sendToAll(Message message) {
		List<String> result = new ArrayList<>();
		ACLMessage aclMsg = new ACLMessage(ACLMessage.INFORM);
    	for(String id : getAgentsList()) {
    		aclMsg.addReceiver(new AID(id, AID.ISLOCALNAME));
    		result.add(id);
    	}
    	aclMsg.setContent(CommunicationManager.serialize(message));
    	send(aclMsg);
    	return result;
	}

	public void sendToOne(Message message, String receiver) {
		ACLMessage aclMsg = new ACLMessage(ACLMessage.INFORM);
    	aclMsg.addReceiver(new AID(receiver, AID.ISLOCALNAME));
    	aclMsg.setContent(CommunicationManager.serialize(message));
    	send(aclMsg);
	}
	
	public List<String> sendToMany(Message message, List<String> receivers) {
		List<String> result = new ArrayList<>();
		ACLMessage aclMsg = new ACLMessage(ACLMessage.INFORM);
    	for(String id : getAgentsList()) {
    		if(receivers.contains(id)) {
    			aclMsg.addReceiver(new AID(id, AID.ISLOCALNAME));
    			result.add(id);
    		}
    	}
    	aclMsg.setContent(CommunicationManager.serialize(message));
    	send(aclMsg);
    	return result;
	}
	
	public ArrayList<String> getAgentsList() {
		ArrayList<String> agentsAL = new ArrayList<>(); 
		AMSAgentDescription[] agents = null;
		SearchConstraints sc = new SearchConstraints();
		sc.setMaxResults(new Long(-1));
		try {
			agents = AMSService.search(this, new AMSAgentDescription(), sc);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		for(int i=0;i<agents.length;++i) {
			String agendtId = agents[i].getName().getLocalName();
			if(agendtId.contains(idMustContain)) {
				agentsAL.add(agendtId);
			}
		}
		return agentsAL;
	}
	
	@Override
	public void log(String message) {
		logger.log(message);
	}

	@Override
	public Robot getRobot() {
		return this.robot;
	}
	
	private class LogHandler {
		private static final String LOG_FOLDER_NAME = "log";
		private FileWriter fileWriter;

		public LogHandler() {
			try {
				File folder = new File(LOG_FOLDER_NAME);
				folder.mkdir();
				fileWriter = new FileWriter(
						new File(LOG_FOLDER_NAME + File.separator + getAgentName() + "-logger.txt"), 
						false);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		public void log(String message) {
			try {
				fileWriter.write(message + System.getProperty("line.separator"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public void delete() {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
